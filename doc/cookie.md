# cookie 说明文档

## hasChosenType

+ 值为 `1`: 已经选择了类型

+ 值为 `2`: 跳过，没有选择类型

+ 其它: 说明此用户是新用户, 还没有进行 类型选择

## firstChoiceType

值为形如: `'aaa, bbb, ccc'` 形式, 需要 `arr.split(',')` 转回数组
