export interface GoodsInfo {
    goodsId: number;
    goodsName: string;
    goodsDesc: string;
    goodsPrice: number;
    goodsImgSrc: string;
}