export interface UserInfo {
    userId: number;
    username: string;
    userLogoSrc: string;
}